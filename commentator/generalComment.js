class GeneralComment {
    constructor({ userLogin, message }) {
        this.userNickname = this.getNickname(userLogin);
        this.message = message;
    }

    getNickname (userLogin) {
        switch (userLogin) {
            case 'demo':
                return `${userLogin} непереможний`;
            case 'test':
                return `${userLogin} безбаговий`;
            case 'happy':
                return `${userLogin} щасливчик`;
            default: return userLogin;
        }
    }
}

module.exports = GeneralComment;