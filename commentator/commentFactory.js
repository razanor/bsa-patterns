const GeneralComment = require('./generalComment');
const SimpleComment = require('./simpleComment');
const messages = { GeneralComment, SimpleComment };

// I believe this also can be defined as the Facade pattern. From the definition, the Facade gives an easy to use interface, without worrying about the complexity of realization.
module.exports = {
    createMessage(type, attributes) {
        const messageType = messages[type];
        // Here a simple Proxy implemented. If somebody tries to call an object with not exisiting key, he will get "Default massage"
        const handler = {
            get: function(target, name) {
                return (
                    name in target ? target[name] : "Default message"
                );
            }
        };
        return new Proxy(new messageType(attributes), handler);
    }
};