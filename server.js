const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const commentator = require('./commentator/commentFactory');
const helpers = require('./helpers/helper');

const texts = require('./data/texts.json');
const messages = require('./data/messages');

require('./passport.config');

server.listen(3000);

passport.serializeUser((user, done) => { done(null, user); });
passport.deserializeUser((user, done) => { done(null, user); });

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());
app.use('/', require('./router/index.routes'));

app.get('/', (req, res) => { res.sendFile(path.join(__dirname, 'index.html')); });
app.get('/game', (req, res) => { res.sendFile(path.join(__dirname, 'game.html')); });
app.get('/login', (req, res) => { res.sendFile(path.join(__dirname, 'login.html')); });

let allUsers = [];

io.on('connection', socket => { 
    socket.on('addUser', payload => {
        const { token } = payload;
        const user = jwt.verify(token, 'someSecret');
        if (user) {
            const userLogin = jwt.decode(token).login;
            const isUnique = allUsers.find(el => el.userLogin === userLogin) ? false : true;
            if (isUnique) {
                allUsers.push({ userLogin, score: 0, winner: false });
                const message = 'На арену виходить новий гравець! Наші вітання' ;
                const comment = commentator.createMessage('GeneralComment', { userLogin, message });
                helpers.socketEmit(socket, 'newUser', { user: userLogin, comment });       
            }
            if (allUsers.length === 1) {
                const randomNum = helpers.randomFunc(texts.length - 1, 0);
                helpers.socketEmit(socket, 'startCountdown', { randomNum });
            }
        }  
    });

    socket.on('trackResults', payload => {
        const { token, text } = payload;
        const user = jwt.verify(token, 'someSecret');
        if (user) {
            const userLogin = jwt.decode(token).login;
            const textLength = text.length;
            const score = text.filter(el => el === '\n').length;
            const userIndex = allUsers.findIndex(el => el.userLogin === userLogin);
            allUsers[userIndex].score = score;
            allUsers[userIndex].winner = score === text.length;
            let leader = helpers.sortUsersArray(allUsers)[0];
            let comment = null;
            let nearFinish = textLength - score;
            if (score % 30 == 0 && nearFinish > 30) {
                comment = commentator.createMessage('GeneralComment', {
                    userLogin: leader.userLogin,
                    message: `Гонка набрала обертів, лідирує`
                });
            }
            if (score % 21 == 0 && nearFinish > 30 ) {
                let randomNum = helpers.randomFunc(messages.length - 1, 0);
                let message = messages.find(message => message.id === +randomNum).body;
                comment = commentator.createMessage('SimpleComment', { message });
            }
            if (nearFinish == 30) {
                let message = `Гонка наближається до завершення. Лідирує - 
                    <strong>${leader.userLogin}</strong>,
                    на другому місці <strong>${helpers.sortUsersArray(allUsers)[1].userLogin}</strong>`; 
                comment = commentator.createMessage('SimpleComment', { message });
            }
            helpers.socketEmit(socket, 'showResults', { allUsers, comment, textLength });
        }
    });

    socket.on('deleteUsers', payload => {
        const { token } = payload;
        const user = jwt.verify(token, 'someSecret');
        if (user) {
            allUsers = [];
        }
    });
});

module.exports = allUsers;
