const randomFunc = (max, min) => (Math.floor(Math.random() * (max - min + 1)) + min);

const sortUsersArray = (allUsers) => ([...allUsers].sort((a, b) => b.score - a.score));

const socketEmit = (socket, ev, data) => {
    socket.broadcast.emit(ev, data);
    socket.emit(ev, data);
};

module.exports = {
    randomFunc,
    sortUsersArray,
    socketEmit
};