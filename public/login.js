window.onload = () => {

    const loginBtn = document.querySelector('#submit');
    const emailField = document.querySelector('#email');
    const pwField = document.querySelector('#password');

    loginBtn.addEventListener('click', () => {
        fetch('/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: emailField.value,
                password: pwField.value
            })
        }).then(res => {
            res.json().then(body => {
                console.log(body);
                if (body.auth) {
                    localStorage.setItem('jwt', body.token);
                    location.replace('/game');
                } else {
                    console.log('auth failed');
                }
            });
        }).catch(err => {
            console.log(err);
        });
    });
};
