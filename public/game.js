
window.onload = () => {

    const usersList = document.querySelector('#users-list');
    const connectBtn = document.querySelector('#connect');
    const countDown = document.querySelector('#countdown');
    const inputFild = document.querySelector('#inputFild');
    const textWrapper = document.querySelector('#textWrapper');
    const messageContainer = document.querySelector('.message');

    const jwt = localStorage.getItem('jwt');
    if (!jwt) {
        location.replace('/login');
    } else {
        // eslint-disable-next-line no-undef
        const socket = io.connect('http://localhost:3000');
        
        connectBtn.addEventListener('click', () => {
            socket.emit('addUser', { token: jwt });
        });
        
        socket.on('newUser', payload => {
            const { user, comment } = payload;
            const { userNickname, message } = comment;
            const newLi = document.createElement('li');
            newLi.innerHTML = `${user}`;
            usersList.appendChild(newLi);
            messageContainer.innerHTML = `${message} - <strong>${userNickname}</strong>`;
        });

        socket.on('startCountdown', payload => {
            const { randomNum } = payload;
            const countDownDate = new Date();
            countDownDate.setSeconds(countDownDate.getSeconds() + 25);
            
            const timer = setInterval(() => {
                const now = new Date();
                let distance = countDownDate.getTime() - now.getTime();
                let seconds = Math.floor((distance % (1000 * 60)) / 1000);
                countDown.innerHTML = `Game will start in ${seconds > 0 ? seconds : 0}s`;
              
                if (distance < 0) {
                    clearInterval(timer);
                    inputFild.style.visibility = 'visible';
                    inputFild.focus();
                    fetchText(randomNum, socket);
                }
            }, 100);
        });

        socket.on('showResults', payload => {
            const { allUsers: players, comment, textLength } = payload;
            if (comment) {
                messageContainer.innerHTML = comment.message;
                messageContainer.innerHTML += comment.userNickname ?
                    `<strong> ${comment.userNickname}</strong>` : '';
            }
            usersList.innerHTML = '';
            players.forEach(player => {
                let newLi = document.createElement('li');
                if (player.winner) {
                    finishGame(player, newLi, socket);
                }
                newLi.innerHTML = `${player.userLogin} <progress id="bar" max="${textLength}"
                value="${player.score}"></progress>`;
                usersList.appendChild(newLi);
            });
        });
    }

    const fetchText = (randomNum, socket) => {
        fetch(`/text?randomNum=${randomNum}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`
            }
        }).then(res => {
            res.json().then(response => {
                const { text: { body: textBody}, comment: { message } } = response;
                textWrapper.innerHTML = textBody;
                messageContainer.innerHTML = message;
                addKeypressListener(textBody, socket);
            });
        }).catch(err => {
            console.log(err);
        });
    };

    const addKeypressListener = (textBody, socket) =>  {
        let text = textBody.split('');
        inputFild.addEventListener('keypress', ev => {
            let textToBeUpdated = textBody.split('');
            let currentElem = text.find(el => el !== '\n');
            let currentIndex = text.indexOf(currentElem);                       
            if (ev.keyCode === currentElem.charCodeAt(0)) {
                if (textToBeUpdated[currentIndex + 1]) {
                    textToBeUpdated[currentIndex + 1] = 
                    `<strong class='success'>${text[currentIndex + 1]}</strong>`;
                    updateTextareaText(textToBeUpdated, text, currentIndex, 'success');
                }
                text[currentIndex] = '\n';
                socket.emit('trackResults', { token: jwt, text });
            } else {
                textToBeUpdated[currentIndex] = `<strong class='failure'>${text[currentIndex]}</strong>`;
                updateTextareaText(textToBeUpdated, text, currentIndex, 'failure');
            }
            textWrapper.innerHTML = textToBeUpdated.join('');
        });
    };

    const finishGame = (player, newLi, socket) => {
        messageContainer.innerHTML = `У нас є переможець. 
            Вітаємо <strong>${player.userLogin}</strong>!
            З вами був Ескейп Ентерович. До нових зустрічей!`;
        newLi.classList.add('winner');
        inputFild.disabled = true;
        socket.emit('deleteUsers', { token: jwt });
        const countDownDate = new Date();
        countDownDate.setSeconds(countDownDate.getSeconds() + 20);
        const timer = setInterval(() => {
            const now = new Date();
            let distance = countDownDate.getTime() - now.getTime();
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);
            document.querySelector('#countdown').innerHTML = `Room will be closed in ${seconds > 0 ? seconds : 0}s`;
        
            if (distance < 0) {
                clearInterval(timer);
                localStorage.removeItem('jwt');
                location.replace('/game');
            }
        }, 100);
    };

    const updateTextareaText = (textToBeUpdated, text, index, className) => {
        const plusOne = className == 'success' ? 1 : 0;
        const correctText = [];
        const breakCondition = `<strong class='${className}'>${text[index + plusOne]}</strong>`;
        for (let i = 0; i < textToBeUpdated.length; i++) {
            if (textToBeUpdated[i + plusOne] == breakCondition) {
                break ;
            } 
            correctText.push(textToBeUpdated[i]);
        }
        inputFild.value = correctText.join('');
    };
};