const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const commentator = require('../commentator/commentFactory');

const users = require('../data/users.json');
const texts = require('../data/texts.json');

router.get('/text', passport.authenticate('jwt'), (req, res) => {
    const text = texts.find(text => text.id === +req.query.randomNum);
    const comment = commentator.createMessage("SimpleComment", {
        message: "Гонка почалась! Побажаємо успіху нашим учасникам"
    });
    res.status(200).json({ text, comment });
});

router.post('/login', (req, res) => {
    const userFromReq = req.body;
    const userInDB = users.find(user => user.email === userFromReq.email);
    if (userInDB && userInDB.password === userFromReq.password) {
        const token = jwt.sign(userInDB, 'someSecret', { expiresIn: '24h' });
        res.status(200).json({ auth: true, token });
    } else {
        res.status(401).json({ auth: false });
    }
});

module.exports = router;